from PyPDF2 import PdfFileWriter, PdfFileReader
from Tkinter import Tk
from tkFileDialog import askopenfilename
import tkMessageBox
import copy, sys

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file

output = PdfFileWriter()
input1 = PdfFileReader(open(filename, "rb"))

print "PDF in question has %d pages." % input1.getNumPages()

for i in range(input1.getNumPages()):
    left_page = input1.getPage(i)
    right_page = copy.copy(left_page)

    print "Original page %d dimensions: (%d, %d) to (%d, %d)" % (i, left_page.mediaBox.getLowerLeft_x(), left_page.mediaBox.getLowerLeft_y(), left_page.mediaBox.getUpperRight_x(), left_page.mediaBox.getUpperRight_y())

    centerX = left_page.mediaBox.getUpperRight_x() / 2
    llx_left = 0
    llx_right = centerX
    urx_left = centerX
    urx_right = left_page.mediaBox.getUpperRight_x()

    lly_left = 0
    lly_right = 0
    ury_left = left_page.mediaBox.getUpperRight_y()
    ury_right = left_page.mediaBox.getUpperRight_y()

    left_page.mediaBox.upperRight = (urx_left, ury_left)
    left_page.mediaBox.lowerLeft = (llx_left, lly_left)
    print "Left page dimensions: %s to %s" % (left_page.mediaBox.lowerLeft, left_page.mediaBox.upperRight)
    output.addPage(left_page)

    right_page.mediaBox.upperRight = (urx_right, ury_right)
    right_page.mediaBox.lowerLeft = (llx_right, llx_left)
    print "Right page dimensions: %s to %s" % (right_page.mediaBox.lowerLeft, right_page.mediaBox.upperRight)
    output.addPage(right_page)

new_filename = (filename[:-4] + "_split.pdf")
outputStream = file(new_filename, "wb")
output.write(outputStream)

tkMessageBox.showinfo(title="Done!", message=("File saved to" + new_filename))
