# Late 19th Century Theatre #

## Developments in Directing ##
- **Adolphe Montigny** - placed a real table onstage as opposed to painting one on the set - forced actors to sit at the table
  - furnished rooms with real furniture and real props that actors had to interact with
  - not considered a director, but one of the first theater managers bringing changes like this toward actors

## Developments in Design ##
- Great effort to try to include details from everyday life onstage
- e.g. _My Friend Fritz_ - premiered at Comedie Francais
  - in a farm setting, in an actual farm yard with running water, cherry tree with real edible cherries, real food and drink onstage
- by the end of the century, the methods of obtaining scenery were pretty much standardized to what we have today - director gives series of requirements to set designers, who would build specific scenery for specific show
  - cardboard models that were approved
  - then scale drawings to guide theater's carpenters
  - then sent to scenic studios for painting
- rise of melodrama/realistic looking spectacle brings this about
- **Henry Irving** - usually considered a producer but responsible for innovations in scenery
  - abandoned grooves/wing and drop system
  - adopted "free plantation" system - can plant scenery wherever you want
    - lessens the amount of perspective, but he gets the praise for popularizing this, similar ideas to Montigny
  - first British producer to make an art out of stage lighting
    - separated footlights and borders into different controllable sections
    - put colored glass in front of lights to change their color
    - black masking pieces in order to prevent light spill to different parts of the stage
    - known for being the first to consistently darken the auditorium (although America had been doing this for a while)
  - production influences
    - rehearsed set changes onstage
    - raked floor
    - employed about 135 people to arrange and conduct scenery

## Developments in Acting ##
- we see realistic touches coming into play
  - mundane things: knitting onstage, smoking, playing cards
- **Charles Kean**
- **Fancois Delsarte** - acting system
  - trying to break acting down into a science and create laws for how to act
  - analyze emotions and ideas and determine how they are best outwardly expressed
  - lists of motions associated with emotions - in an effort to be realistic, based on the character, rather than Restoration ideas
  - first official "movement technique"
- **Sarah Bernhardt** - one of the great actors of the period
  - followed Delsarte's ideas
  - staying in character throughout a whole rehearsal process

## Developments in Playwriting ##
- **Alexander Dumas _fils_** (1824)
  - _The Lady of the Camillias_ - forbidden at first - "too realistic" - most popular play
- Sardou's historical spectacle _Fatherland!_ (1868)
  - Sardou is one of the most popular playwrights
  - his plays were praised for being very realistic and logical
  - George Bernard Shaw hated the mindlessness of his plays

## Developments in Theatre Architecture ##
- typical victorian theater - flat stage, no doors, no boxes
- 
