# French Theatre
1/22/16

## Background
- 1515: Francis I assumes the Throne
  - France feeling the effects of the renaissance from Italy
  - Francis into the arts; invited Italian artists and scholars to his court
  - Idea of going back to older classical texts, rediscovering them and adapting
  - By the end of Francis' reign, they were republishing Terence manuscripts
  - lots of old Roman/Greek texts published and helps to develop a standard for writers
- created a legal standard for plays
- 1547: Henry II reigns, marries Catherine de Medici
  - seven writers form the **Pleiade**, established by government
    - formulate rules on grammar for French language
    - enrich language by inventing new words (partly to avoid calling things by Italian words)
    - illustrate ideals in literary works - adapting plays like Aristotle and Plato into French
    - beginning of idea of French standard for plays
    - not legal power but cultural power
- public stages: run by confraternities
- **Confrerie de la Passion** had a monopoly on French public theatre (organized in 1402)
  - built the **Hotel de Bourgogne** - public theater (Duke of Burgundy donated land for this at the demand of the king)
  - while this was being built, there was a ban on all religious plays (1548) - didn't stop the Confrerie, they leased †he building out to groups performing secular theatre
  - as far as we know, the hotel was used at irregular intervals, without much new work being produced
- court theatre - regals would have court festivals and tournaments
- Catherine de Medici created *Ballet de cour*, equivalent to English masque
  - song and dance w allegorical plot
- 1577: *Commedia dell'arte* troupes arrive
- 1589: Henry of Navarre becomes Henry IV
- 1598: Edict of Names for Protestants: safe area for Protestants to live, French theatrical performances resume
  - early French performances were derivative of *commedia dell'arte*
- **_Commedia dell'arte_** - formulaic shows w/ stock characters, tricky servant, obstructionist, pair of lovers, generic slapstick humor
- Conditions began to change when the *confrerie* gave up participation - gave control over to the city. As a result of theatre coming back and this giving up of control --> led to the creation of more talented artists and works

1/25/16

## **Alexandre Hardy** (c. 1572-1632)
- says he wrote 500 plays, scholars believe this to be a lie (more like around 60), we have 34
- wanted to adapt his plays for audiences
- mindful of renaissance ideal - standards put forth by Aristotle
  - interested in five act form (vs. Lope's 3) which developed from old Greek plays
  - used other neoclassical devices e.g. poetic dialogue
  - moved away from some devices, e.g. didn't observe unity of time or place, allowed violence onstage
- never published work until late in life - under contract to an acting company
- never really achieved greatness
- came into theatre in an age when comedy was everything, but his plays focused on tragedy and did some genre blending
  - brought about support for tragedies in this era
  - his success encouraged other to write further plays
- worked for company run by **Valleran LeComte** - France's first successful manager

## Theatre companies and performance spaces
- LeComte's was the best company in Paris, of a number of companies that came and went
  - his company was known as the **King's Company** - the king wasn't very powerful at the time, but he got the king's approval for that title with no benefits
  - most highly touted company in Paris
- there were a lot of French companies - we can identify 400 in this period
- no company was allowed to settle in Paris w/o the king's permission
- early performance spaces
  - **_Hotel de Bourgogne_** 1548 - we believe it was about 40 ft wide x 105 ft long
  - no permanent seats in earliest versions
  - there was a pit
  - around the auditorium were the boxes/galleries (*loges*)
  - usually at the back was an *amphitheatre*, raised seating facing the stage. left undivided. cheaper seating
  - highest area was the *paradis* - above the *loges* - this was where "business" was conducted. also families would send servants to reserve seats, then paid for the servants to sit up here
  - stage was raised about 6 ft above the pit and occupied almost the full width of the building. we don't know how deep it was, prob between 17-35
- LeComte left a lot of records - we know how much he paid for each aspect
- Hardy's plays required between 3-7 different locations, we can determine info about the stage from that too
- locations suggested by backdrop probably
- *Confrerie* had a monopoly and got money from every troupe in Paris
- for hotels that didn't perform at the Hotel, they did perform at a tennis court
- **_Le jeu de paume_** = tennis
  - tennis courts were rented out as a theatre - had the same basic dimensions as a theatre and had seating over on on side which was used as boxes
  - upper level for standing only
  - number of tennis courts in Paris ranged from 250 to 1800
  - 30 ft by 90 ft - little smaller than Hotel but not much
  - windows below the roof provided ample light
- not really any formal police organizations - nobody cared if fights broke out in the pit - sometimes actors would stop and watch the fight
- could fit 1000 people into these pits

## More Background
- Louis XIII becomes king in 1610
- his mother set up an advisor to help the king and run the state
- for Louis, that was Cardinal Richelieu - was the power behind the throne
  - his focus was to center all the power of France in the crown
- when Louis comes to the throne, new kind of stability and money, and that's when the arts thrive

### **Jean de Mairet**
- first tragedy following neoclassical rules - *Sophonisba* (1634)
- helped keep interest in tragedy going after Hardy
- quickly fell out of favor thanks to Cornielle

## **Neoclassical Rules for tragedy *(Les règles)***
- critics at the time didn't see themselves as reviewers but theorists who set the standards which playwrights followed
- dramatists gave in and agreed to follow Greek and Roman models
- wanted to subtly adapt them to be French somehow
- **subject matter**
  - Greeks limited themselves to myths and legends
  - French writers did that but allowed themselves leeway to deal with more contemporary issues
- **mechanics**
  - Greek tragedies: 3 actors
  - French had small cast lists but definitely more than 3
- **cast ensemble**
  - Greeks: all male
  - French: had female actresses
- French didn't have choruses (didn't find them believable, didn't fit on stage, expensive)
- role of the chorus was onstage embodied in the role of the **_confidant_**, e.g. the nurse or the friend/associate
- main influence came to the adaptation of the rules from Horace's *Ars Poetica* - became *Les règles* i.e. The Rules
  - a letter where Horace is writing about and summarizing Aristotle (i.e. they didn't actually use Aristotle's writing directly, but instead Horace's paraphrasing)
  - **_The Three Unities_** _(Aristotle's ideas which became law in France)_
    - **unity of action**
      - (only one plot w/ no subplots)
    - **unity of time**
      - (plays take place over the span of one "revolution of the sun")
      - legal disputes over 12 hours vs 24 hours
      - real time was considered best
    - **unity of place**
      - Aristotle doesn't emphasize this as much
      - all action should take place in one location
      - legal debates - one town is okay, one set location is ideal
      - would take plays and try to fit them into one setting - cramming multiple _simultaneous settings_ into one stage
      - 1/27/16
- **_raisemblance_** - the idea of verisimilitude (the appearance of truth) - the most central unifying principle - doesn't say that it has to be true, just has to have the appearance of truth. three prerequisites:
  - **generality** - truth is not found in details but in archetypes/norms/objectively looking at society
    - supporting larger more general truths - virtue is rewarded
    - purity of dramatic types - tragedy should deal with regal characters, kings act in a regal way and do not misbehave
    - has to be about kings because they have further to fall
    - as a result, tragedy should be poetic and lofty because that's how kings speak
    - tragedy has to end unhappily because someone is getting punished for doing something
    - comedy has to end happily and feature lower class characters, and characters speak in the vernacular
  - **morality** - plays need to articulate the moral patterns of the world
    - god is omnipotent
    - poetic justice must always be done
    - there needs to be a lesson taught to the audience
    - comedy must ridicule bad behavior, tragedy should show consequences of bad behavior
  - **reality** - nothing on stage that would not happen in real life - getting rid of supernatural
    - exception: if they're adapting a myth or biblical story involving the supernatural - still frowned upon and should still be minimized
    - got rid of the chorus because that is not real
    - got rid of soliloquies
    - confidant as a role
    - violence usually kept offstage (they had a hard time making it look real) except for slapstick stuff
- **_les bienséance_** - decorum, properties, what is fitting
  - e.g. a king would never have an affair - a king always acts just, the king is right, does not err, always speaks beautiful language
- **_liason de scènes_** (linking of scenes) - "French scenes" - new scenes any time anyone enters or exits - continuous course of action with no breaks between scenes within an act
- **Alexandrine verse** - 12 syllables, every 3rd syllable stressed
  - play would not get produced if it didn't meet these requirements
- one of the few examples in history where art is dictated from the top down


### **Pierre Cornielle**
- *Melite* (1629)
- *Le Cid* (1637) - best known of his work
  - based off of a larger six-act play, the actual story itself is an even bigger story
  - 2/1/16
  - key idea with all French tragedy is restraint
  - greater force in something that is confined/restricted - restrictions that people can fight against
  - lot of tension onstage magnified by characters fighting to contain their emotions
  - **_gloire_** = glory/honor (French equivalent to Spanish _pundonor_)
  - doesn't observe unity of time (technically 24 hours but impossible)
  - violence onstage - slap
  - no _liaison de scenes_
  - there are soliloquies in the play - Cornielle's arguments was that it's a prayer
  - play doesn't fit any specific kind of drama
  - play is more remembered for its historical influence rather than its literary merit

## French Comedy

### **Jean-Baptiste Polqueline (Molière)**  (1622-1673)
- father expected him to become an upholsterer
- no trace of performing heritage anywhere in his family
- 1643, he signed a contract to join a company with the Bejart family, had to change his name to avoid embarrassing his family
  - Mme. Bejart became his mistress and eventually his mother-in-law
  - leased a tennis court in Paris, unfortunately picked a theater in a bad part of town
  - chose the name "The Illustrious Theatre"
  - competition from other unofficial theaters, forced to close after one yet (Two years left on the lease)
  - bankrupt - caused them to leave town to avoid prosecution
  - left for 12 years, during this time became a father figure to the company
  - eventually became the controller and spokesman for the company
  - worked on a sharing system - got extra shares as playwright and actor
- while he was touring, the king's brother the duke saw a Molière production and gave them money, invited them to come back to Paris and perform for Louis XIV
  - they choose to perform a tragedy called _Nicomede_, it had been recently performed at Hotel de Bourgogne
  - Molière described as a small man with a wimpy voice, apparently performed it naturally, but his comedy was highly criticized
  - the show is awful, king is not interested, but Molière does another show real quick - one of his comedies (_Le Docteur Amoureux_)
  - Louis loved it so much that he gave Molière's company their own theater - **_Petit Bourbon_**
- **_Petit Bourbon_**
  - 45 feet wide, 221 feet long
  - two elevated galleries
  - wasn't a very good place for drama (length/distance from stage)
  - eventually torn down
- next Moliere went to the **_Palais Royale_**
  - built by Richelieu, remodeled by Torrelli
  - more traditional type space - proscenium arch with wing and drop system
  - reputation mostly derived from Moliere and his success
  - remembered for his plays of character - he acted in his own plays as well
- wrote many different plays, also wrote for court
- wrote comedy ballets with **Jean-Baptiste Lully**
  - dialogue scenes alternate with ballet portions (not necessarily singing)
  - dance numbers took the same function as musical numbers
  - Lully known as the founder of French opera
  - born in Italy, moved to France at 12
  - successful as a musician, became a court musician
  - within 8 years of appointment, became the superintendent of all court music
  - began working with Moliere in this capacity
  - had experience writing for ballets, machine plays, other forms
  - would view theater that people wanted, adapted it to the king's taste and kept the thing up to date
  - the king loved him, gave him a monopoly of all musical performances in Paris - any musical performances had to pay Lully
  - wanted more and more power: when Moliere died, he took control of the _Palais Royale_ and kicked out his company, turned it into the Royal Academy of Music and Dance (aka the Opera)
  - put an end to machine plays once he controlled music - banned music from machine plays
- **machine play** - play where you watch scenery/machinery change - occasionally some dialogue, but focus on elaborate scenery (music along with it)
- Moliere's company eventually disbanded and died
- Moliere tried to write machine plays and tragedies, neither of which were successful

2/3/15
### **Jean Racine** (1639-1699)
- most successful tragic writer in France
- most notable play: _Phaedra_ (1677)
- follows the idea of restraint - his plays are examples of how the concept of restraint can succeed

## Early Acting Companies
- we see an increased stability in acting, encouraging more people to act
- first troupe - allowed to settle in Hotel de Bourgogne - Valleran's troupe
  - most important person: **Pierre le Messier Bellerose**
  - Valleran died in 1613, Bellerose took over, toured Paris but came back to Hotel
  - known as one of France's first great actors, excelled in comedy and tragedy
- rival acting company headed up by **Guillaume des Gilberts Montdory**
  - won the favor of Richelieu
  - originally an apprentice at Valleran's company
  - his company performed _Le Cid_ - now Montdory was considered great French actor (except he only excelled at tragedy)
  - Theatre du Mairet
  - troupe faded away after Montdory suffered partial paralysis
- by 1640, troupes are struggling - Bourgogne and Mairet are both fading
- King Louis formed the company - **The King's Company**
  - from this moment on, official acting troupes got money from king
  - raised reputation of actors in society
  - stated "the actor's profession should not be considered worthy of blame"
  - made it easier to be an actor - abolished legal restrictions
- however, church was still very much against theatre
  - actors were not allowed to be baptized, married, death rights
  - believed actors were doomed to hell
  - varied form parish to parish
  - common understanding that actors would recant after retiring or on their deathbed
  - very important to be buried on consecrated ground
- king created **Comedie Francais** (sp?) - closed Theatre du Mairet, took best actors from all fading troupes and formed his own theater - world's first national theatre
  - given a monopoly on all spoken drama - had to pay them money to be allowed to perform a play
- organization of acting companies
  - organized on a sharing plan
  - numbers varied:
    - before king's support size was 8-12
    - king's troupe had 27 members, 21 shares (17 people had one full share, 7 half shares, 3 1/4 shares), shares varied according to actor's role in the company
  - dictated by French courts - decided that not all actors would be sharing members, only a number of _societaires_ set by courts, when someone dies the societaires elect a new one
  - second group - salaried actors hired yearly (_pensionnaires_)
  - to become a pensionnaire you have to play a variety of tragic and comedic roles - most pensionnaires never become societaires
  - societaires have a 20 year contract with the theater - there's a fine if you leave early (or die)
  - societaires had a say in all policy including play selection - leader was just who had been with the company the longest
  - if you stayed there 20 years, retired with an annual pension from the government
  - usually cast to play a type, although it's nice to be able to play a variety
  - cast of a show decided by the playwright who sold their play to the company - partially because plays were written with specific actors in mind
    - societaires were never allowed to refuse a role
    - societaires cast the older, classical plays
  - rehearsed for the morning of the show - actor was expected to have 30-50 (or up to 70) shows in repertoire at any time
  - once a play was performed, expected to have it ready at any time thereafter
  - typically, not expected to be word perfect, would mix in other things they knew - not an evolved convention for realistic acting yet in France
  - authors paid a fixed sum for each play - once they handed it over, as long as it was unpublished it was the property of the company
    - playwrights wanted them published so they could make money, but then other companies would just perform/steal it (no copyright yet)
    - actors would go watch plays and copy them for rival companies
  - expenses of company were subtracted before actors were paid
    - paid salaried people before shares were paid out
  - most characters dressed in appropriate contemporary garments, e.g. Roman generals would wear French general outfits
  - scenic artists were not so well paid

## Scenery
- scenery becoming more and more in demand
- perspective scenery on stage - actors couldn't stand too far back or they would break the realism of the scenery - audiences loved the perspective
  - Hotel de Bourgogne - stage was 45 ft deep, floor was raked (to assist with perspective)
  - mostly, actors were on the flat apron of the stage so they could be seen and heard
  - only lights were chandelier candles above the audiences and footlights at the apron of the stage
  - acting was all about voice and presentation and the lead actor
  - tragedy had a big arch but nobody could interact with it because of perspective scenery
- new type of machine play buildings such as _Salle du Machines_ (1662)
  - one building with five miles of garden perspective for this purpose

2/8/16

### Jean Racine / _Phaedra_
- idea of people who are subject to their own passions - Phaedra is not at fault for what happens to her, which makes it tragic
- 
